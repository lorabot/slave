"""Receive data test"""
import logging
from datetime import datetime
from contextlib import closing

from core import LoraRN2483, LORA_OK
from slave.drive import LoraBotDrive


logger = logging.getLogger(__name__)


def main():
    with LoraRN2483(timeout=None) as lora:
        with closing(LoraBotDrive()) as drive:
            lora.init_p2p()
            last_ts = datetime.now()
            while True:
                try:
                    rec = lora.send_command('radio rx 0')[0]
                    if rec != LORA_OK:
                        logger.error('Failed to start receiving %s', rec)
                        continue
                    data = lora.readline().decode().strip()
                    if data == 'radio_err':
                        logger.error('Radio error')
                        continue
                    rec_ts = datetime.now()
                    msg_ms = (rec_ts - last_ts).microseconds * 1e-3
                    value = int(data.replace('radio_rx ', ''), 16)
                    logger.debug('Received %s (%s) in %sms', value, data, msg_ms)
                    if msg_ms <= 100:
                        drive.lora_remote(data.replace('radio_rx', '').strip())
                    else:
                        drive.stop()
                    last_ts = datetime.now()
                except:
                    logger.exception('Unexpected crash')
                    raise


if __name__ == '__main__':
    main()
