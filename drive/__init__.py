"""Robot drive.
Raspberry pi -----i2c----> PCA9685 -----pwm-----> MDD10A ---> DC motors.

Use PCA9685 to control all pins in MDD10A. This way there is no need
to wire GPIO of the PI and the controll is unified (all control singals
go through PCA9685.
"""
import re
import logging
from time import sleep

from Adafruit_PCA9685 import PCA9685
from core import messages

MAX_TICK = 4095
RIGHT = 'r'
LEFT = 'l'


logger = logging.getLogger(__name__)


def _pct_to_tick(pct):
    """Returns tick value for on/off in PCA9685."""
    if pct >= 100:
        return MAX_TICK
    if pct <= 0:
        return 0
    return (MAX_TICK * pct) // 100


class LoraBotDrive:
    """LoraBot drive.
    Allows control of robot movement.
    Direction, speed, acceleration can be adjusted.

    Usage:
        drive = LoraBotDrive()
        drive.drive(speed)    # drive at ``speed`` (accelerate gradualy)
        drive.stop()  # stop gradualy
        drive.halt()  # stop imediately
        drive.turn(LEFT)    # turn left
        drive.turn(RIGHT)   # turn right
    """
    DIR = {LEFT: 0, RIGHT: 2}
    PWR = {LEFT: 1, RIGHT: 3}

    def __init__(self, i2c_addr=0x40, pwm_freq=500, del_time=0.1):
        self._pwm = PCA9685(i2c_addr)
        self._pwm.set_pwm_freq(pwm_freq)
        self.speed = {LEFT: 0, RIGHT: 0}
        self.del_time = 0.1
        logger.debug(
            'Initiated with i2c_addr=%s, pwm_freq=%s, del_time=%s',
            hex(i2c_addr), pwm_freq, del_time)

    def _set_dir(self, side, fwd=True):
        """Set direction of movement for ``side``.
        if ``fwd`` is True direction is forward
        otherwise direction is backwards.
        """
        self._pwm.set_pwm(self.DIR[side], 0, MAX_TICK * fwd)

    def _set_speed(self, side, speed):
        """Set speed of the drive.

        ``side`` - left or right - drive to be adjusted.
        ``speed`` - (-100, 100) - new speed of the drive.
        """
        self._set_dir(side, speed >= 0)
        self._pwm.set_pwm(self.PWR[side], 0, _pct_to_tick(abs(speed)))
        self.speed[side] = speed

    def _adjust_speed(self, side, target_speed, step_size):
        """Change speed of the ``side`` by at most ``step_size``
        set to ``target_speed`` if closer then ``step_size`` from
        current speed.

        Example:
            current speed = 30 -> _adjust_speed(60, 10) will
            change speed to 40

            current speed = 30 -> _adjust_speed(35, 10) will
            change speed to 35
        """
        diff = target_speed - self.speed[side]
        step = step_size if abs(diff) >= step_size else abs(diff)
        try:
            sign = diff // abs(diff)
            new_speed = self.speed[side] + (step * sign)
        except ZeroDivisionError:
            new_speed = self.speed[side]
        self._set_speed(side, new_speed)

    def _tank_turn(self, side, strength, step_size, del_time=None):
        """Spin tracks in opposite directions.
        ``strength`` is the maximum speed that tracks will spin at (0-100).
        ``step_size`` is the step by which the speed is incremented.
        ``del_time`` is the delay between each step.
        Only works if both tracks are at rest
        """
        logger.debug(
            'Takn turn, side=%s, strength=%s, step_size=%s, del_time=%s',
            side, strength, step_size, del_time)
        if self.speed[LEFT] or self.speed[RIGHT]:
            logger.debug('Abort tank turn')
            return
        del_time = del_time or self.del_time
        l_trgt = -strength if side == LEFT else strength
        r_trgt = -strength if side == RIGHT else strength
        while self.speed[LEFT] != l_trgt and self.speed[RIGHT] != r_trgt:
            self._adjust_speed(LEFT, l_trgt, step_size)
            self._adjust_speed(RIGHT, r_trgt, step_size)
            sleep(del_time)
        self.stop()

    def close(self):
        """For compliance with ``contextlib.closing``"""
        self.halt()

    def halt(self):
        """Full stop both drives without decelration"""
        self._set_speed(LEFT, 0)
        self._set_speed(RIGHT, 0)
        logger.debug('Halted')

    def drive(self, speed, step_size=10, del_time=None):
        """Drive at ``speed`` (-100 to 100).
        Speed is adjusted ``step_size`` at a time with ``del_time`` seconds
        delay in between.
        If robot was turning it will start driving straight.
        """
        logger.debug(
            'Drive with speed=%s, step_size=%s, del_time=%s',
            speed, step_size, del_time)
        del_time = del_time or self.del_time
        while self.speed[LEFT] != speed or self.speed[RIGHT] != speed:
            self._adjust_speed(LEFT, speed, step_size)
            self._adjust_speed(RIGHT, speed, step_size)
            sleep(del_time)

    def stop(self, step_size=10, del_time=None):
        """Decrement by step_size (percentage) untill full stop"""
        self.drive(0, step_size, del_time or self.del_time)

    def turn(self, side, strength=50, step_size=10, del_time=None):
        """Adjust direction to the ``side``.
        ``strength`` control how rapid the turn is.
        If robot is in straight motion then turning left(or right) will
        decrease left (or right) track by ``strength`` percent.
        if robot is stationary (not moving) it will accelerate
        both tracks to ``strength`` in opposite direction (tank
        like movement) and stop
        If robot is already turing, do nothing.
        """
        logger.debug(
            'Turn, side=%s, strength=%s, step_size=%s, del_time=%s',
            side, strength, del_time)
        if self.speed[LEFT] != self.speed[RIGHT]:
            logger.debug('Abort turn')
            return
        if self.speed[LEFT] == self.speed[RIGHT] == 0:
            logger.debug('Switch to tank turn')
            self._tank_turn(side, strength, step_size, del_time)
        del_time = del_time or self.del_time
        scale = strength / 100
        cruise_speed = self.speed[side]
        slow_speed = cruise_speed - int(cruise_speed * scale)
        while self.speed[side] != slow_speed:
            self._adjust_speed(side, slow_speed, step_size)
            sleep(del_time)
        while self.speed[side] != cruise_speed:
            self._adjust_speed(side, cruise_speed, step_size)
            sleep(del_time)

    def lora_remote(self, lora_data):
        """Remote control via LoRa.
        ``lora_data`` is a data string from lora receiver.
        """
        logger.debug('Lora remote request: %s', lora_data)
        cmd, *args = [x for x in re.findall('..', lora_data)]
        args = [int(x, 16) for x in args]
        if cmd == messages.HALT:
            self.halt()
        elif cmd == messages.STOP:
            self.stop(*args)
        elif cmd == messages.TURN:
            side = RIGHT if (args[0] & 0b10000000) else LEFT
            strength = args[0] & 0b1111111
            self.turn(side, strength, *args[1:])
        elif cmd == messages.DRIVE:
            speed = (args[0] & 0b1111111) * (1 if args[0] & 0b10000000 else -1)
            self.drive(speed, *args[1:])
